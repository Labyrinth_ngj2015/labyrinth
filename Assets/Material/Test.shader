﻿Shader "Custom/AlmostToonEdge" {
    Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _EdgeCutoff ("Edge cutoff" , range(0,1) ) = 0.1
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200
       
        CGPROGRAM
        #pragma surface surf Lambert
 
        sampler2D _MainTex;
        float _EdgeCutoff;
 
        struct Input {
            float2 uv_MainTex;
            float3 viewDir;
            float3 worldNormal;
        };
 
        void surf (Input IN, inout SurfaceOutput o) {
            float colors = 6;
            o.Emission = 0;
            half4 c = tex2D (_MainTex, IN.uv_MainTex);
            if( dot( IN.viewDir , IN.worldNormal ) > _EdgeCutoff )
            {
                c.r = floor(c.r*colors + 0.5)/colors;
                c.g = floor(c.g*colors + 0.5)/colors;
                c.b = floor(c.b*colors + 0.5)/colors;
                o.Emission = c.rgb;
            }
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
 
}