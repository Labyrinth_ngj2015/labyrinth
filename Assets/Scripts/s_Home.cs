﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class s_Home : MonoBehaviour {

    public Tipo tipo;
    public Text textStart;

	// Use this for initialization
	void Start () {
        s_Settings.tipo = tipo;
        if (tipo == Tipo.server)
            textStart.text = "Start Game!";
        else
            textStart.text = "Join Game!";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GoToGame()
    {
        Application.LoadLevel("GameScene");
    }
}
