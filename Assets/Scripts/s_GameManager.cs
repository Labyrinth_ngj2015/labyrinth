﻿using UnityEngine;
using System.Collections;

public class s_GameManager : MonoBehaviour {

    public GameObject floor;
    public GameObject sky;

	// Use this for initialization
	void Start () {
        GenerateSky();
        GenerateFloor();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void GenerateFloor()
    {
        float startX = -s_Settings.FLOOR_SIZE_X / 2 * 0.32f + 0.16f;
        float startY = -s_Settings.FLOOR_SIZE_Y / 2 * 0.32f + 0.16f;
        for(int i = 0; i < s_Settings.FLOOR_SIZE_X; i++)
        {
            for(int j = 0; j < s_Settings.FLOOR_SIZE_Y; j++)
            {
                Vector3 pos = new Vector3(startX + i * 0.32f, startY + j * 0.32f, 0);
                Instantiate(floor, pos, Quaternion.identity);
            }
        }
    }

    private void GenerateSky()
    {
        float startX = -s_Settings.SKY_SIZE_X / 2 * 1.5f + 0.75f;
        float startY = -s_Settings.SKY_SIZE_Y / 2 * 1.5f + 0.75f;
        for (int i = 0; i < s_Settings.SKY_SIZE_X; i++)
        {
            for (int j = 0; j < s_Settings.SKY_SIZE_Y; j++)
            {
                Vector3 pos = new Vector3(startX + i * 1.5f, startY + j * 1.5f, 0);
                Instantiate(sky, pos, Quaternion.identity);
            }
        }
    }
}
