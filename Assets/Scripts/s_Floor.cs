﻿using UnityEngine;
using System.Collections;

public class s_Floor : MonoBehaviour {

    private SpriteRenderer _spriteRenderer;

	// Use this for initialization
	void Start () {
        _spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        clientController clientC = col.gameObject.GetComponent<clientController>();
        if (clientC != null && clientC.IsMine ()) 
		{
			_spriteRenderer.color = new Color (0.62f, 0.73f, 1f);
		}
    }
}
