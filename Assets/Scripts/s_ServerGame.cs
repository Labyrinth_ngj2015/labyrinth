﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class s_ServerGame : MonoBehaviour {

    public Camera _camera;
    private int _playerCount;
    public GameObject playerPrefab; //this is just for the client
    private bool _playerSpawned;
    private bool _paused;
    public Text countDown;
    private bool _started;

	// Use this for initialization
	void Start () {
        countDown.text = "WAITING FOR PLAYERS!!";
        _paused = true;
        _playerSpawned = false;
        _started = false;
        _playerCount = 0;
        if (s_Settings.tipo == Tipo.server)
        {
            Network.InitializeServer(s_Settings.CONNECTION_ALLOWED, s_Settings.CONNECTION_PORT, !Network.HavePublicAddress());
            //MasterServer.RegisterHost(s_Settings.GAME_NAME, s_Settings.GAME_ROOM);
        }
        else
        {
            NetworkConnectionError error = Network.Connect("192.168.137.1", s_Settings.CONNECTION_PORT);
            if (error != 0)
                BackToHome();
            _camera.enabled = false;
            //_paused = false;
            //GameObject player = GameObject.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        }
	}

    private Vector3 GetRandomSpawn()
    {
        int rand = (int) Random.Range(0, 4);
        float x = 0;
        float y = 0;
        switch(rand)
        {
            case 0:
                x = 4.95f;
                y = 3.7f;
                break;
            case 1:
                x = -4.95f;
                y = 3.7f;
                break;
            case 2:
                x = -4.95f;
                y = -3.7f;
                break;
            case 3:
                x = 4.95f;
                y = -3.7f;
                break;
            default:
                x = 4.95f;
                y = 3.7f;
                break;
        }
        return new Vector3(x, y, 0);
    }

    void OnServerInitialized()
    {
        Debug.Log("Server started");
    }
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    void OnPlayerConnected(NetworkPlayer player)
    {
        Debug.Log("player connected "+_playerCount);
        networkView.RPC("SpawnPlayer", RPCMode.OthersBuffered);
        _playerCount++;
        if(!_started && _playerCount >= s_Settings.PLAYER_TO_START)
        {
            _paused = false;
            networkView.RPC("StartCoundDown", RPCMode.OthersBuffered);
            StartCoundDown();
            _started = true;
        }
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        _playerCount--;
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
        Debug.Log("player disconnected");
    }

    [RPC]
    private void SpawnPlayer()
    {
        if (!_playerSpawned && s_Settings.tipo == Tipo.client)
        {
            Network.Instantiate(playerPrefab, GetRandomSpawn(), Quaternion.identity, 0);
            _playerSpawned = true;
            Debug.Log("player spawned");
        }
    }

    public bool IsPause()
    {
        return _paused;
    }

    public void MazeLose()
    {
        _paused = true;
        networkView.RPC("ShowLostMaze", RPCMode.OthersBuffered);
    }

    [RPC]
    public void ShowLostMaze()
    {
        if (s_Settings.tipo == Tipo.server)
        {
            countDown.text = "YOU LOSE!!!";
            StartCoroutine(EndGame());
        }
    }

    [RPC]
    public void BackToHome()
    {
        Application.LoadLevel("Home");
    }

    [RPC]
    public void StartCoundDown()
    {
        countDown.text = "3";
        StartCoroutine(Minus2());
    }

    private IEnumerator MasterWin()
    {
        yield return new WaitForSeconds(s_Settings.MATCH_TIME);
        countDown.text = "YOU WIN!!!";
        StartCoroutine(EndGame());
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject player in players)
        {
            player.GetComponent<clientController>().LosePlayer();
        }
    }

    private IEnumerator EndGame()
    {
        yield return new WaitForSeconds(2f);
        Application.LoadLevel("Home");
    }

    private IEnumerator Minus2()
    {
        yield return new WaitForSeconds(1f);
        countDown.text = "2";
        StartCoroutine(Minus1());

    }

    private IEnumerator Minus1()
    {
        yield return new WaitForSeconds(1f);
        countDown.text = "1";
        StartCoroutine(StartGame());
    }

    private IEnumerator StartGame()
    {
        yield return new WaitForSeconds(1f);
        countDown.text = "GO!";
        StartCoroutine(ClearScreen());
    }

    private IEnumerator ClearScreen()
    {
        yield return new WaitForSeconds(1f);
        _paused = false;
        StartGamePlayer();
        countDown.text = "";
    }

    public void StartGamePlayer()
    {
        if(s_Settings.tipo == Tipo.server)
            StartCoroutine(MasterWin());
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach(GameObject player in players)
        {
            player.GetComponent<clientController>().SetPause(false);
        }
    }
}
