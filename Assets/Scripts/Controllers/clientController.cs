﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class clientController : MonoBehaviour {

    public float _speed = 0.5f;
    public float _stopTreshold = 0.02f;
    private Quaternion initialRotation;
    private bool _paused;
    private Text victoryText;

    void Awake()
    {
        initialRotation = transform.rotation;
    }
	// Use this for initialization
	void Start () {
        victoryText = GameObject.FindGameObjectWithTag("victoryText").GetComponent<Text>();
        _paused = GameObject.FindGameObjectWithTag("gameManager").GetComponent<s_ServerGame>().IsPause();
        if(networkView.isMine)
            transform.GetChild(1).gameObject.SetActive(true);
        if(networkView.isMine)
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.blue;
        else
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = Color.green;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!_paused)
        {
            transform.rotation = initialRotation;
            if (networkView.isMine)
            {
                Vector3 direction = new Vector3();
                direction.y = Input.acceleration.y;
                direction.x = Input.acceleration.x;
                Quaternion _endRotation = Quaternion.LookRotation(direction, Vector3.forward);
                rigidbody2D.AddForce(direction * _speed, ForceMode2D.Force);

				if(Input.GetKeyDown(KeyCode.S))
				{
					transform.position = new Vector2(transform.position.x, transform.position.y + (100 * Time.deltaTime));
				}
				else if( Input.GetKeyDown(KeyCode.W))
				{
					transform.position = new Vector2(transform.position.x, transform.position.y - (100 * Time.deltaTime));
				}

                //var z = transform.rotation.eulerAngles.z;
                //transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(z, _endRotation.eulerAngles.z, Time.deltaTime * 2));

//				Vector3 playerPosition = _player.transform.position;
//				Vector3 direction = _target.transform.position - playerPosition;
//				
//				// calculates the angle we should turn towards, - 90 makes the sprite rotate
//				float targetAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 90;
//				transform.rotation = Quaternion.Euler (0, 0, targetAngle);
            }
            Vector2 vel = rigidbody2D.velocity;
            if (vel.magnitude > 0.1f)
            {
                rigidbody2D.velocity = vel * 0.1f;
            }
        }
	}

    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        Vector3 syncPosition = Vector3.zero;
        if (stream.isWriting)
        {
            syncPosition = rigidbody.position;
            stream.Serialize(ref syncPosition);
        }
        else
        {
            stream.Serialize(ref syncPosition);
            rigidbody.position = syncPosition;
        }
    }

    public bool IsMine()
    {
        return networkView.isMine;
    }

    public void Win()
    {
        _paused = true;
        if(networkView.isMine)
            victoryText.text = "YOU WIN!";
        networkView.RPC("Lose", RPCMode.OthersBuffered);
        GameObject.FindGameObjectWithTag("gameManager").GetComponent<s_ServerGame>().MazeLose();
        //StartCoroutine(EndGame());
    }

    public void SetPause(bool set)
    {
        _paused = set;
    }
    
    [RPC]
    public void Lose()
    {
        Debug.Log("Lose");
        _paused = true;
        if(!networkView.isMine)
            victoryText.text = "YOU LOSE :(";
        StartCoroutine(EndGame());
    }

    private IEnumerator EndGame()
    {
        yield return new WaitForSeconds(2f);
        Network.Disconnect();
        Application.LoadLevel("Home");
    }

    public void LosePlayer()
    {
        networkView.RPC("Lose", RPCMode.OthersBuffered);
    }
}
