﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class WallMovements : MonoBehaviour {

    private Quaternion _initialRotation;
    private Quaternion _endRotation;
    private int iteration;
    private bool _canClick;
    public float[] availableRotations;
    private int _currentRotation;

    void Start()
    {
        _canClick = true;
        _initialRotation = transform.rotation;
        _endRotation = _initialRotation;

        float z = _initialRotation.eulerAngles.z;
        _currentRotation = (availableRotations.Length > 0 && availableRotations.Contains(z))
                            ? _currentRotation = availableRotations.ToList().IndexOf(z)
                            : 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!_canClick)
        {
            var z = transform.rotation.eulerAngles.z;
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Lerp(z, _endRotation.eulerAngles.z, Time.deltaTime * 2));
            if (Mathf.Abs(z - transform.rotation.eulerAngles.z) < 0.01)
            {
                _canClick = true;
                transform.rotation = Quaternion.Euler(0, 0, _endRotation.eulerAngles.z);
            }
        }
    }

    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        Quaternion syncRotation = Quaternion.identity;
        if (stream.isWriting)
        {
            syncRotation = transform.rotation;
            stream.Serialize(ref syncRotation);
        }
        else
        {
            stream.Serialize(ref syncRotation);
            transform.rotation = syncRotation;
        }
    }

    void OnMouseDown()
    {
        if (_canClick && availableRotations.Length > 0)
        {
            _currentRotation = (++_currentRotation) % availableRotations.Length;
            _endRotation = Quaternion.Euler(0, 0, availableRotations[_currentRotation]);
            _canClick = false;
        }
    }
}