﻿using UnityEngine;
using System.Collections;

public class s_Arrow : MonoBehaviour {

	private GameObject _player;
	private GameObject _target;

	public float fixedDistance = 1;

	// Use this for initialization
	void Start () {
		_player = transform.parent.gameObject;
		_target = GameObject.FindWithTag ("target");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPosition = _player.transform.position;
		Vector3 direction = _target.transform.position - playerPosition;

		// calculates the angle we should turn towards, + 90 makes the sprite rotate
		float targetAngle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 90;
		transform.rotation = Quaternion.Euler (0, 0, targetAngle);

//		float distFromTarget = Mathf.Sqrt (Mathf.Pow (direction.x, 2) + Mathf.Pow (direction.y, 2));
//		float dx = direction.x * fixedDistance / distFromTarget;
//		float dy = Mathf.Pow (fixedDistance, 2) - Mathf.Pow (dx, 2);
//
//		Vector2 newPosition = new Vector2 (playerPosition.x + dx, playerPosition.y + dy);
//		transform.position = newPosition;
	}
}
