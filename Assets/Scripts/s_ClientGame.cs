﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class s_ClientGame : MonoBehaviour {

    public GameObject playerPrefab;
    private HostData[] _hostList;

	// Use this for initialization
	void Start () {
        //MasterServer.RequestHostList(s_Settings.GAME_NAME);
        //Network.Connect("192.168.137.1", s_Settings.CONNECTION_PORT);
        GameObject.Instantiate(playerPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity);
	}

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnMasterServerEvent(MasterServerEvent msEvent)
    {
        /*
        if (msEvent == MasterServerEvent.HostListReceived)
        {
            _hostList = MasterServer.PollHostList();
        }
         * */
        Debug.Log("super before");
        Network.Connect("192.168.137.1", s_Settings.CONNECTION_PORT);//(_hostList[0]);
        Debug.Log("super after");
    }

    void OnConnectedToServer()
    {
        Debug.Log("Client connected");
        SpawnPlayer();
    }

    void OnServerInitialized()
    {
        
    }

    private void SpawnPlayer()
    {
        Network.Instantiate(playerPrefab, new Vector3(0f, 0f, 0f), Quaternion.identity, 0);
    }

	
}
