﻿using UnityEngine;
using System.Collections;

public enum Tipo { server, client };

public static class s_Settings {

    public const float MATCH_TIME = 60;
    public const int CONNECTION_ALLOWED = 4;
    public const int CONNECTION_PORT = 8080;
    public const string GAME_NAME = "NGJ15Labyrinth";
    public const string GAME_ROOM = "Room1";
    public const int FLOOR_SIZE_X = 32;
    public const int FLOOR_SIZE_Y = 24;
    public const int SKY_SIZE_X = 12;
    public const int SKY_SIZE_Y = 6;
    public static Color[] P_COLOR = { Color.red, Color.blue, Color.green, Color.yellow };
    public static Tipo tipo = Tipo.client;
    public const int PLAYER_TO_START = 0;
}
